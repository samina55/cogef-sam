# creates: 1scogef.png

from ase import io
from ase.optimize import FIRE
from ase.calculators.emt import EMT

from cogef import COGEF

image = io.read('AuAg2.traj')
image.calc = EMT()

fmax = 0.01
cogef = COGEF(0, 2, optimizer=FIRE, optimizer_logfile=None, fmax=fmax)

if not len(cogef.images):  # calculation was done already
    cogef.images = [image]

    stepsize = 0.1
    steps = 30
    cogef.pull(stepsize, steps)
