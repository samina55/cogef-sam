from cogef import COGEF
from cogef import Dissociation
from cogef.units import nN

cogef = COGEF(0, 2)
diss = Dissociation(cogef)

LOADING_RATE = 10  # [nN/s]
P = 101325.  # Pressure [Pa]

fstep = 0.003
for T in [5, 50, 100, 200, 300, 500]:  # Temperature [K]
    fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                       method='electronic')
    force, error = diss.rupture_force_and_uncertainty(
        T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
    print('Rupture force ({0}K): ({1:4.2f} +- {2:4.2f}) nN'.format(
        T, force / nN, error))
