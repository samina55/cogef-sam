# creates: 1scogef_problem.png
from ase.atoms import Atoms
from ase.optimize import FIRE
from ase.calculators.morse import MorsePotential
from cogef import COGEF

fmax = 0.05

image = Atoms('H10', positions=[(i, 0, 0) for i in range(10)])
image.calc = MorsePotential()
FIRE(image, logfile=None).run(fmax=fmax)

cogef = COGEF(0, 8, optimizer=FIRE, optimizer_logfile=None, fmax=fmax)
stepsize = 0.03
if len(cogef.images) == 0:
    cogef.images = [image]
    steps = 35
    cogef.pull(stepsize, steps)
