# creates: 1scogef_problem.png
import matplotlib.pyplot as plt
from cogef import COGEF

cogef = COGEF(0, 8)
energies, distances = cogef.get_energy_curve()

plt.plot(distances, energies)
plt.xlabel('d [$\\AA$]')
plt.ylabel('U [eV]')
plt.savefig('1scogef_problem.png')
