# creates: 1scogef_error.png
import matplotlib.pyplot as plt
from ase.units import J, m
from ase.io.jsonio import read_json
from ase.calculators.morse import MorsePotential

from cogef import COGEF
from cogef.cogef2d.fixed2d import FixedForce2D

cogef = COGEF(0, 8)
cogef2 = COGEF(0, 1, optimizer_logfile=None)


def initialize(image):
    image.calc = MorsePotential()
    return image


cogef3s = FixedForce2D(cogef, cogef2, initialize)

stepsize = 0.01
cogef3s.find_barrier(stepsize, range(1, 20))
forces, barriers = cogef3s.collect_barriers()
plt.plot(forces / J * m * 1e9, barriers, 'o-', label='3S-COGEF')

with open('1scogef_barriers.json') as f:
    dct = read_json(f)
plt.plot(dct['forces'], dct['barriers'], '-', label='1S-COGEF')

plt.legend()
plt.xlabel('force [nN]')
plt.ylabel('barrier [eV]')
# plt.show()
plt.savefig('1scogef_error.png')
